    $(function(){
        
        var Person = Backbone.Model.extend({

            validation: {
        firstname:{
             required: true,
             pattern: /^[a-zA-Z ]{2,30}$/,
        },
        lastname: {
            required: true,
            pattern: /^[a-zA-Z ]{2,30}$/,
        },
        gender: {
            required: true,
            range: [0, 1]
        },
        age: {
            required: true,
            range: [0, 120]
            }
            }
        });

        var SignUpView = Backbone.View.extend({
          events: { 
            "click #validate": function (e) {
             e.preventDefault();
            this.validate();
           }
          },

          initialize: function(){
            var self = this;
            this.firstname = $("#firstname");
            this.lastname = $("#lastname");
            this.age = $("#age");
            this.gender = $('#gender');

            
            this.firstname.change(function(e){
              self.model.set({firstname: $(e.currentTarget).val()});
            });
            this.lastname.change(function(e){
              self.model.set({lastname: $(e.currentTarget).val()});
            });
            this.age.change(function(e){
              self.model.set({age: $(e.currentTarget).val()});
            });
            this.gender.change(function(e){
              self.model.set({gender: $(e.currentTarget).val()});

            });
            Backbone.Validation.bind(this);
          },

          validate: function(){
            var errors = this.model.validate();
            if(errors === undefined ){
              alert('Great Success!');
              console.log(new Date().toLocaleString() +" " + 'Validation OK');
            }
            else{
              alert('Wrong data!');
              console.log(new Date().toLocaleString() +" "+ 'Validation FAILED');
              for (var er in errors) {
                console.log(errors[er]);
              }
            }


          },
            
          remove: function() {
        Backbone.Validation.unbind(this);
        return Backbone.View.prototype.remove.apply(this, arguments);
    }
        });



      window.SignUpView = new SignUpView({
         el: $("#info"),
         model: new Person()});
      });
